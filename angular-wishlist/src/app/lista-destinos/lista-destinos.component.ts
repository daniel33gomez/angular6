import { Component, OnInit, Output, EventEmitter } from '@angular/core';
// import { DestinoViajeComponent } from '../destino-viaje/destino-viaje.component';
import { DestinoViaje } from './../models/destino-viajes.model'
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates: string[];

  // destinos: DestinoViaje[];
  constructor(public destinosApiClient:DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    // this.destinos = [];
    this.updates = [];
    this.destinosApiClient.suscribeOnChange((d: DestinoViaje) => {
      if (d != null ){
        this.updates.push('se ha elegido a ' + d.nombre);
      }
    });
   }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    // this.destinos.push(new DestinoViaje(nombre, url))
    // console.log(this.destinos);
    // return false;
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje){
    // this.destinosApiClient.getAll().forEach(function(x) {x.setSelected(false);});
    // d.setSelected(true);
    this.destinosApiClient.elegir(e)
  }

}
