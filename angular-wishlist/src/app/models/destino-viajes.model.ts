export class DestinoViaje {
    // nombre:string;
    // imagenUrl:string; // No se usa porque se declararon como Public

    private selected: boolean;
    public servicios: string[];
    public id;

    constructor(public nombre:string, public u:string){
        this.servicios = ["Desayuno", "Almuerzo"];
        // Public realiza la misma funcion que las líneas siguientes
        // this.nombre = n;
        // this.imagenUrl = u;
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
}
